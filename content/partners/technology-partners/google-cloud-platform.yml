---
  title: GitLab on Google Cloud
  description: Unite teams and workflows with GitLab, a complete DevOps platform to build, test, and deploy on Google Cloud.
  components:
    - name: partners-call-to-action
      data:
        title: GitLab on Google Cloud
        inverted: true
        body:
          - text: Unite teams and workflows with GitLab, a complete DevOps platform to build, test, and deploy on Google Cloud.
        image:
          image_url: /nuxt-images/partners/gcp/google_cloud.svg
          alt: Google Cloud Logo
    - name: copy-media
      data:
        block:
          - header: One interface for every use case
            text: |
              As the DevOps platform, GitLab lowers the barrier of entry for cloud native solutions like Kubernetes, Knative, and Istio. Simplify your multicloud, hybrid cloud, or all-in-the-cloud deployment strategy with one solution for everyone on your pipeline.

              Receive a $300 sign-up credit for every new GCP account associated with your business email. Plus, new and existing accounts can redeem an additional $200 partner credit to get started with GitLab on Google Cloud Platform.
            video:
              video_url: https://player.vimeo.com/video/631354871?h=d9eed483fd
            miscellaneous: Bronwyn Hastings, VP Technology Ecosystem on Google’s partnership with GitLab to deliver digital transformations for customers.
            link_href: https://cloud.google.com/solutions/modern-cicd-anthos-user-guide
            link_text: Read the whitepaper
            secondary_link_href: https://cloud.google.com/partners/partnercredit?pcn_code=0014M00001h35gDQAQ%3Futm_campaign%3D2018_cpanel&utm_source=gitlab&utm_medium=referral
            secondary_link_text: Get $200 in partner credit
    - name: 'partners-feature-showcase'
      data:
        header: Develop better cloud native applications faster with GitLab and GCP
        text: GitLab’s tight integrations with Google Cloud services enable workflows for every workload. Shrink cycle times by driving efficiency at every stage of the software development process. From idea to production on Google Cloud, GitLab’s complete DevOps platform delivers built-in planning, monitoring, and reporting solutions for modern applications.
        cards:
          - header: Collaborate practically
            text: Iterate faster, transform together. Modern CI/CD with Anthos reduces rework so happier developers and cloud practitioners can deliver product roadmaps instead of repairing old roads.
          - header: Automate securely
            text: Lock up your process. Automated DevSecOps workflows increase uptime by reducing security and compliance risks on Google Cloud infrastructure.
          - header: Celebrate repeatedly
            text: Deliver when it matters, where it matters. Increase market share and revenue when your product is on budget, on time, and always up on GCP.
    - name: 'benefits'
      data:
        header: Get started with GitLab and GCP joint solutions
        full_background: true
        cards_per_row: 2
        text_align: left
        benefits:
          - no_slippers_icon: /nuxt-images/partners/gcp/google-kubernetes-engine.png
            title: Google Kubernetes Engine (GKE)
            description: GKE is Google’s managed Kubernetes service, designed to automate deployment, scaling, and management of containerized Linux and Windows applications. With GitLab’s GKE integration, teams can quickly provision new GKE clusters or import existing clusters in just a few clicks. Leverage GitLab’s Auto DevOps functionality for the lowest barrier of entry to deploy container workloads to GKE with CI/CD.
            link:
              text: Learn more
              url: /blog/2020/03/27/gitlab-ci-on-google-kubernetes-engine/
          - no_slippers_icon: /nuxt-images/partners/gcp/anthos.png
            title: Anthos
            description: Anthos is a modern application platform that provides a consistent development and operations experience for on-premise and cloud environments. GitLab supports GKE On Premise (GKE-OP), CloudRun for Anthos, and Anthos Configuration Management for workflow optimization on top of Anthos' unified infrastructure management platform. Plus, GitLab supports on-prem GKE for hybrid cloud customers. Together, GitLab with Anthos provides enterprises with consistency and scalability across heterogeneous environments.
            link:
              text: Learn more
              url: https://cloud.google.com/solutions/partners/deploying-gitlab-anthos-gke-on-prem-cicd-pipeline
          - no_slippers_icon: /nuxt-images/partners/gcp/google-cloud-run.png
            title: Cloud Run
            description: Cloud Run is a fully managed serverless platform that automatically scales stateless containers and abstracts away all infrastructure management. Deploy to Cloud Run with GitLab Serverless – a full CI/CD workflow to build and test serverless applications. With GitLab for Cloud Run, teams can streamline and simplify serverless management on any infrastructure (Knative, Cloud Run, Cloud Run for Anthos, etc.) through a single UI.
            link:
              text: Learn more
              url: https://docs.gitlab.com/ee/user/project/clusters/serverless/
          - no_slippers_icon: /nuxt-images/partners/gcp/google-compute-engine.png
            title: Google Compute Engine
            description: Google Compute Engine (GCE) delivers configurable, high-performance virtual machines running in Google’s data centers. GitLab CI/CD provides application delivery to virtual machines as deployment targets. Migrate traditional, non-containerized workloads to the cloud with GitLab. Get started by installing GitLab on a single GCE instance or in High Availability architecture.
            link:
              text: Learn more
              url: https://docs.gitlab.com/ee/install/google_cloud_platform/
          - no_slippers_icon: /nuxt-images/partners/gcp/google-app-engine.png
            title: Google App Engine
            description: Google Compute Engine (GCE) delivers configurable, high-performance virtual machines running in Google’s data centers. GitLab CI/CD provides application delivery to virtual machines as deployment targets. Migrate traditional, non-containerized workloads to the cloud with GitLab. Get started by installing GitLab on a single GCE instance or in High Availability architecture.
            link:
              text: Learn more
              url: https://medium.com/faun/deploy-directly-from-gitlab-to-google-app-engine-d78bc3f9c983
          - no_slippers_icon: /nuxt-images/partners/gcp/goolge-cloud-functions.png
            title: Google Cloud Functions
            description: Google Cloud Functions (GCF) is Google Cloud’s event-driven serverless compute platform. Store your code in GitLab SCM and directly deploy as cloud functions through GitLab CI/CD. Empower your teams to adopt GCP for a more event-driven, cloud native architecture with GitLab and GCF by, for example, automating development for Firebase and Cloud Functions.
            link:
              text: Learn more
              url: https://cloud.google.com/functions
          - no_slippers_icon: /nuxt-images/partners/gcp/google_firebase_image.png
            title: Firebase
            description: Firebase is a platform for creating mobile and web applications developed by Google. Together, GitLab SCM and CI help developers automate with first-class CI/CD pipelines to build, test, and deploy updates frequently to the entire Firebase stack.
            link:
              text: Learn more
              url: /blog/2020/03/16/gitlab-ci-cd-with-firebase/
    - name: 'pull-quote'
      data:
        quote: We had developers that thought, Why would we do something else? Jenkins is fine. But I think those people need to see GitLab first and see what the difference is because GitLab is so much more than Jenkins. The power of GitLab is you can do so much more and you can make everything so much easier to manage.
        source: MICHIEL CREFCOEUR, FRONTEND BUILD AND RELEASE ENGINEER AT ANWB
        link_text: ''
        shadow: true
    - name: copy-resources
      data:
        title: Discover the benefits of GitLab on GCP
        block:
          - video:
              title: 'Opening Keynote: The Power of GitLab - Sid Sijbrandij'
              video_url: https://www.youtube.com/embed/xn_WP4K9dl8?enablesjsapi=1
              label: Gitlab commit virtual 2020
            resources:
              blog:
                header: Blogs
                links:
                  - text: Kubernetes and the future of cloud native - We chat with Kelsey Hightower
                    link: /blog/2019/05/13/kubernetes-chat-with-kelsey-hightower/
                  - text: How to leverage GitLab CI/CD for Google Firebase
                    link: /blog/2020/03/16/gitlab-ci-cd-with-firebase/
                  - text: CloudRun for Anthos
                    link: /blog/2019/11/19/gitlab-serverless-with-cloudrun-for-anthos/
