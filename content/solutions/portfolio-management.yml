---
  title: Portfolio Management
  description: GitLab's portfolio management allows you to manage large scale organization wide projects.
  components:
    - name: 'solutions-hero'
      data:
        title: Portfolio Management
        subtitle: Manage large scale organization-wide projects
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        rounded_image: true
        primary_btn:
          text: Start your free trial
          url: /free-trial/
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          hide_in_mobile: true
          alt: "Image: gitlab for portfolio management"
    - name: 'solutions-feature-list'
      data:
        title: Tracking multiple active programs while staying abreast of portfolio performance
          is challenging.
        subtitle: GitLab Portfolio Management helps organizations plan, track and measure
          velocity of multiple programs aligned with business initiatives.
        icon:
          name: gitlab
          alt: GitLab Icon
          variant: marketing
        features:
          - title: Manage large programs
            description: |
              Large programs have various work streams that need to be planned, tracked and measured to ensure the programs are on schedule.

              *   With [multi-level](https://docs.gitlab.com/ee/user/group/epics/){data-ga-name="multi level epics" data-ga-location="body"} epics with multiple child epics and their associated issues, organizations can maintain visibility and control while prioritizing initiatives that deliver the maximum value.
              *   Organize epics and issues by dragging and dropping in the epic tree to prioritize the work.
            icon:
              name: ci-cd
              alt: CI-CD Icon
              variant: marketing
              hex_color: '#9B51DF'
            image_url: /nuxt-images/solutions/portfolio-management/multi-level-epic.png
            image_alt: Multi Level Epics at GitLab
            image_tagline: |
              [Multi Level Epics](https://docs.gitlab.com/ee/user/group/epics/){data-ga-name="epics" data-ga-location="body"}
          - title: Visualize and measure performance of large programs
            description: |
              Establish product vision, strategy and roadmap as well as gain insight into how your cross functional program is progressing with a [portfolio level roadmap](https://docs.gitlab.com/ee/user/group/roadmap/){data-ga-name="roadmap" data-ga-location="body"} view.

              *   Identify, report on and quickly respond to the [health of individual issues and epics](https://docs.gitlab.com/ee/user/project/issues/index.html#health-status){data-ga-name="health status" data-ga-location="body"} by viewing the health status.
              *   [Epic boards](https://docs.gitlab.com/ee/user/group/epics/epic_boards.html){data-ga-name="epic boards" data-ga-location="body"} allow you to visualize and track your epics and their workflows in a Kanban-style board
            icon:
              name: source-code
              alt: Source Code Icon
              variant: marketing
              hex_color: '#52CDB7'
            image_url: /nuxt-images/solutions/portfolio-management/epic_view_roadmap.png
            image_alt: Portfolio Level Roadmap at GitLab
            image_tagline: |
              [Portfolio Level Roadmap](https://docs.gitlab.com/ee/user/group/roadmap/){data-ga-name="portfolio level roadmap" data-ga-location="body"}
