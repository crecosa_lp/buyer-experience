---
  title: Solutions
  description: GitLab is The One DevOps platform that empowers organizations to deliver software faster, more efficiently, while strengthening security and compliance.
  content_config:
    column_size: 4
    header_animation: fade-up
    header_animation_duration: 500
  hero:
    title: GitLab Solutions
    subtitle: GitLab is The One DevOps platform that empowers organizations to deliver software faster, more efficiently, while strengthening security and compliance.
    primary_btn:
      text: Talk to an expert
      url: /sales/
      data_ga_name: talk to an expert
      data_ga_location: hero
    secondary_btn:
      url: /pricing/
      text: Learn about pricing
      data_ga_name: pricing
      data_ga_location: hero
      variant: ghost
  cta_divider:
    text: Explore more GitLab features
    href: /features/
  navigation:
    hide_link: true
    header: 
      text: From planning to production, bring teams together in one application.
      variant: heading4
    buttons: 
      - text: Continuous Integration & Delivery
        icon_left: automated-code
        icon_right: arrow-down
        href: '#continuous-integration'
      - text: GitOps
        icon_left: digital-transformation
        icon_right: arrow-down
        href: '#gitops'
      - text: Security & Compliance
        icon_left: devsecops
        icon_right: arrow-down
        href: '#security-and-compliance'
      # - text: Governance
      #   icon_left: shield-check-light
      #   icon_right: arrow-down
      #   href: /solutions/delivery-automation/
      - text: Agile Planning
        icon_left: agile-alt
        icon_right: arrow-down
        href: '#agile-planning'
      - text: Value Stream Management
        icon_left: visibility
        icon_right: arrow-down
        href: '#value-stream-management'
      - text: Source Code Management
        icon_left: cog-code
        icon_right: arrow-down
        href: '#source-code-management'
      - text: Software Supply Chain Security
        icon_left: lock-cog
        icon_right: arrow-down
        href: '#software-supply-chain-security'
      # - text: Analytics & Dashboards
      #   icon_left: monitor-test
      #   icon_right: arrow-down
      #   href: /solutions/continuous-software-security-assurance/
  feature_blocks:
    - header:
        title: Continuous Integration & Delivery
        icon: automated-code
        subtitle: Build, maintain, deploy, and monitor complex pipelines with advanced CI/CD. GitLab automates all the steps required to build, test, and deploy your code to your production environment.
        button:
          text: Learn more about Continuous Integration & Delivery
          url: /features/continuous-integration/
        list:
          - Improve developer productivity with in-context testing results.
          - Create secure and compliant releases by testing every change.
          - Automate consistent pipelines for both simplicity and scale.
          - Accelerate your cloud native and hybrid cloud journey
      id: continuous-integration
      cards:
        - title: "Parent-child pipelines"
        - title: "Protected variables"
        - title: "Merge Trains"
        - title: "Canary Deployments"
        - title: "Auto DevOps"
        - title: "Multi-project pipeline graphs"
        - title: "Environments and deployments"
    - header:
        title: GitOps
        icon: digital-transformation
        subtitle: Foster collaboration between your infrastructure, operations, and development teams so you can deploy more frequently with greater confidence. GitLab gives you version control, code review, and CI/CD in a single application for a seamless experience.
        button:
          text: Learn more about GitOps
          url: /solutions/gitops/
        list:
          - A single app for SCM and CI/CD
          - Tightly integrated with Terraform and Vault
          - Trusted by the world's largest engineering teams
      id: gitops
      cards:
        - title: "GitLab Agent for Kubernetes"
        - title: "GitOps deployment management"
        - title: "Fine-grained access controls for CI/CD based Kubernetes deployments"
        - title: "Auto Deploy to EC2 with Auto DevOps"
        - title: "Terraform plan output summary in Merge Requests"
        - title: "GitLab-managed Terraform state files"
        - title: "Pipeline status visible in pull/merge request"
    - header:
        title: Security & Compliance
        icon: devsecops
        subtitle: Integrating security into your DevOps lifecycle is easy with GitLab. Security and compliance are built in, out of the box, giving you the visibility and control necessary to protect the integrity of your software.
        button:
          text: Learn more about Security & Compliance
          url: /solutions/dev-sec-ops/
        list:
          - Actionable security and compliance findings with every commit
          - End-to-end transparency of audit events
          - Vulnerability management for developers and security pros
          - Automate policies with compliant pipelines and common controls
          - One platform for comprehensive application security
      id: security-and-compliance
      cards:
        - title: "Compliance pipeline configuration"
        - title: "Streaming Audit Events"
        - title: "Project Dependency List"
        - title: "Chain of custody report"
        - title: "Compliance report"
        - title: "Security Dashboards"
        - title: "Dynamic Application Security Testing"
        - title: "Multiple approvers in code review"

    - header:
        title: Agile Planning
        icon: agile-alt
        subtitle: Whatever your chosen methodology, you can plan and manage your projects, programs, and products with integrated Agile support. 
        button:
          text: Learn more about Agile Planning
          url: /solutions/agile-delivery/
        list:
          - A single application for planning, deployment and beyond
          - Manage projects in the same system where you do the work
          - Customize out-of-the-box functionality to the needs of your methodology

      id: agile-planning
      cards:
        - title: "Roadmaps"
        - title: "Multiple Issue Assignees"
        - title: "Issue Board Milestone Lists"
        - title: "Single level Epics"
        - title: "Scoped Labels"
        - title: "Single level Epics"

    - header:
        title: Value Stream Management
        icon: visibility
        subtitle: Visualize, measure, and manage your software development and delivery workflows so your team can deliver greater customer and business value.
        button:
          text: Learn more about Value Stream Management
          url: /solutions/value-stream-management/
        list:
          - Visualize your end-to-end DevOps workstream.
          - Identify and fix inefficiencies in your development process.
          - Drive continuous improvement based on the data from your value stream.
      id: value-stream-management
      cards:
        - title: "Value Stream Management"
        - title: "Project Level Value Stream Analytics"
          display_title: Value Stream Overview Dashboard
        - title: "DORA Metrics: Metrics overview in Value Stream Analytics"
          display_title: DORA4 Metrics Analytics
        - title: "Group Level Value Stream Analytics"
          display_title: Customizable Value Stream Analytics
        - title: "Contribution Analytics"
        - title: Issue Analytics
        - title: "Insights"
    - header:
        title: Source Code Management
        icon: cog-code
        subtitle: Improve collaboration and productivity with modern source code management and version control.
        button:
          text: Learn more about Source Code Management
          url: /stages-devops-lifecycle/source-code-management/
        list:
          - Scale your SDLC for cloud native adoption
          - Git-based repository enables developers to work from a local copy
          - Automatically scan for code quality and security with every commit
          - Built-in Continuous Integration and Continuous Delivery
      id: source-code-management
      cards:
        - title: "Code Owners"
        - title: "Approval rules for code review"
        - title: "Push rules"
  
    - header:
        title: Software supply chain security
        icon: lock-cog
        subtitle: Combine compliance controls and security scans to protect your internal and external source code. Ensure that builds are done in a secure environment and signed so that others can be confident in their authenticity.
        button:
          text: Learn more about software supply chain security
          url: /solutions/supply-chain/
        list:
          - An end-to-end secure platform to protect multiple attack surfaces
          - Internal and external source code protection
          - Build execution protection
          - Support for SLSA compliance
      id: software-supply-chain-security
      cards:
        - title: "Chain of custody report"
        - title: "Required Merge Request Approvals"
        - title: "X.509 Signed Commits and Tags"
        - title: "Vulnerability Management"
        - title: "Project Dependency List"
        - title: "Protected Runners"
  next-step:
    text: Try Ultimate for Free

   