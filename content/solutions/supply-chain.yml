---
  title: Software Supply Chain Security
  description: Secure your software supply chain, stay ahead of threat vectors, and establish policies to aid compliance adherence so you can deliver secure software faster.
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        note: 
          -  Built-in automation and policy enforcement
        title: Software Supply Chain Security
        subtitle: Secure your software supply chain, stay ahead of threat vectors, and establish policies to aid compliance adherence so you can deliver secure software faster.
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Try Ultimate for Free
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: hero
        secondary_btn:
          text: Learn about pricing
          url: /sales/
          data_ga_name: sales
          data_ga_location: hero
        image:
          image_url: /nuxt-images/resources/resources_11.jpeg
          image_url_mobile: /nuxt-images/supply-chain-hero.png
          alt: "Image: Software supply chain security"
          bordered: true
    - name: 'by-industry-intro'
      data:
        intro_text: Trusted by
        logos:
          - name: Bendigo and Adelaide Bank Logo
            image: /nuxt-images/logos/bendigo_and_adelaide_bank.svg
            url: /customers/bab/
            aria_label: Link to Bendigo and Adelaide Bank customer case study
          - name: Hackerone Logo
            image: /nuxt-images/logos/hackerone-logo.png
            url: /customers/hackerone/
            aria_label: Link to Hackerone case study
          - name: new10 Logo
            image: /nuxt-images/logos/new10.svg
            url: /customers/new10/
            aria_label: Link to The Zebra customer case study  
          - name: The Zebra Logo
            image: /nuxt-images/logos/zebra.svg
            url: /customers/thezebra/
            aria_label: Link to The Zebra customer case study
          - name: Chorus Logo
            image: /nuxt-images/logos/chorus.svg
            url: /customers/chorus/
            aria_label: Link to Chorus customer case study
          - name: Hilti Logo
            image: /nuxt-images/logos/hilti_logo.svg
            url: /customers/hilti/
            aria_label: Link to Hilti customer case study
    - name: 'side-navigation'
      links:
        - title: Overview
          href: '#overview'
        - title: Capabilities
          href: '#capabilities'
        - title: Customers
          href: '#customers'
        - title: Pricing
          href: '#pricing'
        - title: Resources
          href: '#resources'
      slot_enabled: true
      slot_offset: 1
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content: 
            - name: 'by-solution-benefits'
              offset: 2
              data:
                title: Secure your end-to-end software supply chain
                is_accordion: false
                items:
                  - icon:
                      name: continuous-integration
                      alt: Continuous Integration Icon
                      variant: marketing
                    header: Protect your software development lifecycle
                    text: Protect multiple attack surfaces, including your code, build, dependencies, and release artifacts
                    link_text: Learn more about DevSecOps
                    link_url: /solutions/dev-sec-ops
                    ga_name: reduce security learn more
                    ga_location: benefits
                  - icon:
                      name: devsecops
                      alt: Devsecops Icon
                      variant: marketing
                    header: Adhere to compliance requirements
                    text: Easy access to audit and governance reports
                    link_text: Why GitLab
                    link_url: /why-gitlab/
                    ga_name: free up learn more
                    ga_location: benefits
                  - icon:
                      name: shield-check-alt
                      alt: Shield Check Icon
                      variant: marketing
                    header: Implement guardrails
                    text: Control access and implement policies
                    link_text: Learn more about our platform approach
                    link_url: /solutions/devops-platform/
            - name: 'solutions-video-feature'
              offset: 2
              data:
                video: 
                  url: 'https://player.vimeo.com/video/762685637?h=f96e969756'
        - name: 'div'
          id: 'capabilities'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-solutions-block'
              offset: 2
              data:
                subtitle: Code, build, release. Securely.
                sub_description: ''
                white_bg: true
                markdown: true
                sub_image: /nuxt-images/solutions/supply-chain.png
                solutions:
                  - title: Establish zero trust
                    description: |
                      Identity and access management (IAM) is one of the biggest attack vectors in the software supply chain. Secure access with GitLab by authenticating, authorizing, and continuously validating all human and machine identities operating in your environment.
                      *  Implement granular [access control](https://docs.gitlab.com/ee/user/admin_area/settings/visibility_and_access_controls.html) including [2 factor authentication](https://docs.gitlab.com/ee/security/two_factor_authentication.html)
                      *  Establish [token expiration policies](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
                      *  Set up [policies](https://docs.gitlab.com/ee/administration/compliance.html#policy-management) as per organizational or regulatory rules
                      *  Generate comprehensive [audit and governance reports](https://docs.gitlab.com/ee/administration/audit_reports.html) for compliance adherence
                      *  Enforce [two-person approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/rules.html) for additional guardrails
                  - title: Secure your source code
                    description: |
                      Ensure the security and integrity of your source code by managing who has access to the code and how changes to the code are reviewed and merged.

                      *  Establish version control, [code history](https://docs.gitlab.com/ee/user/project/repository/git_history.html?_gl=1*1ngzpgw*_ga*NTg0MjExODQyLjE2MTk1MzkzOTQ.*_ga_ENFH3X7M5Y*MTY2NDUzNDg3My4xMjkuMS4xNjY0NTM4MjQ3LjAuMC4w), and [access control](https://docs.gitlab.com/ee/user/admin_area/settings/visibility_and_access_controls.html) to your source code
                      *  Use automated [code quality](https://docs.gitlab.com/ee/ci/testing/code_quality.html) tests to analyze the performance impact of changes
                      *  Enforce review and [approval rules](https://docs.gitlab.com/ee/ci/testing/code_quality.html) to control what goes into production 
                      *  Run [automated security scans](https://docs.gitlab.com/ee/user/application_security/) to capture vulnerabilities before your code is merged
                      *  Ensure passwords and sensitive information are not in your source code through automated [secrets detection](https://docs.gitlab.com/ee/user/application_security/secret_detection/)
                      *  Implement [signed commits](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/) to prevent developer impersonation
                  - title: Secure dependencies
                    description: |
                      Verify that all open source dependencies used in your projects contain no disclosed vulnerabilities, come from a trusted source, and have not been tampered with.

                      *  Generate a [software bill of materials](https://docs.gitlab.com/ee/user/application_security/dependency_list/) in an automated manner to identify your projects’ dependencies
                      *  Automatically identify vulnerabilities in any dependent software used through automated [software composition analysis](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/)
                      *  Run [license compliance](https://docs.gitlab.com/ee/user/compliance/license_compliance/) scans to ensure your project is using software with licenses within your organization’s policies
                  - title: Secure build environments
                    description: |
                      Prevent bad actors from injecting malicious code into the build process and gaining control over the software built by the pipeline or access to secrets used in the pipeline.
                      
                      *  [Isolate your build environment](https://docs.gitlab.com/runner/security/?_gl=1*1d95r9z*_ga*NTg0MjExODQyLjE2MTk1MzkzOTQ.*_ga_ENFH3X7M5Y*MTY2NDUzNDg3My4xMjkuMS4xNjY0NTM4MDA2LjAuMC4w) to prevent unauthorized access or malicious code execution
                      *  Maintain [release evidence](https://docs.gitlab.com/ee/user/project/releases/#release-evidence) for everything that is included in the release
                      *  Ensure your build artifacts are not compromised with [build artifact attestation](https://docs.gitlab.com/ee/ci/runners/configure_runners.html#artifact-attestation)
                  - title: Secure release artifacts
                    description: |
                      Stop attackers from exploiting weaknesses in an application’s design or configurations to steal private data, gain unauthorized access to accounts, or impersonate legitimate users.

                      *  Establish a [secure connection](https://about.gitlab.com/blog/2022/01/07/gitops-with-gitlab-using-ci-cd/#meet-the-cicd-tunnel) with your cluster to deliver your release artifacts 
                      *  Identify [security vulnerabilities in running applications](https://docs.gitlab.com/ee/user/application_security/dast/) before deploying
                      *  Ensure your [API interfaces](https://docs.gitlab.com/ee/user/application_security/api_fuzzing/) do not expose your running application
        - name: 'div'
          id: 'customers'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-quotes-carousel'
              offset: 1
              data:
                align: left
                header: |
                  Trusted by enterprises.
                  <br />
                  Loved by developers.
                quotes:
                  - title_img:
                      url: /nuxt-images/logos/bendigo_and_adelaide_bank.svg
                      alt: bending and adelaide bank Logo
                    quote: We now have an always-innovating solution that aligns with our goal of digital transformation.
                    author: Caio Trevisan, Head of Devops Enablement, Bendigo and Adelaide Bank
                    ga_carousel: bending and adelaide bank testimonial
                    url: /customers/bab/
                  - title_img:
                      url: /nuxt-images/logos/new10.svg
                      alt: New10 Logo
                    quote: GitLab is really helping us in our very modern architecture, because you're supporting Kubernetes, you're supporting serverless, and you are supporting cool security stuff, like DAST and SAST. GitLab is enabling us to have a really cutting edge architecture.
                    author: Kirill Kolyaskin, CTO
                    ga_carousel: New10 testimonial
                    url: /customers/new10/
                  - title_img:
                      url: /nuxt-images/logos/zebra.svg
                      alt: The Zebra Logo
                    quote: The biggest value (of GitLab) is that it allows the development teams to have a greater role in the deployment process. Previously only a few people really knew how things worked, and now pretty much the whole development organization knows how the CI pipeline works, can work with it, add new services, and get things into production without infrastructure being the bottleneck.
                    author: Dan Bereczki,  Sr. Software Manager, The Zebra
                    url: /customers/thezebra/
                    ga_carousel: the zebra testimonial
                  - title_img:
                      url: /nuxt-images/logos/hilti_logo.svg
                      alt: Hilti Logo
                    quote: GitLab is bundled together like a suite and then ships with a very sophisticated installer. And then it somehow works. This is very nice if you're a company which just wants to get it up and running.
                    author: Daniel Widerin, Head of Software Delivery, HILTI
                    url: /customers/hilti/
                    ga_carousel: hilti testimonial
        - name: div
          id: 'pricing'
          slot_enabled: true
          slot_content:
            - name: 'tier-block'
              class: 'slp-mt-96'
              id: 'pricing'
              offset: 1
              data:
                header: Which tier is right for you?
                cta:
                  url: /pricing/
                  text: Learn more about pricing
                  data_ga_name: pricing
                  data_ga_location: free tier
                  aria_label: pricing
                tiers:
                  - id: free
                    title: Free
                    items:
                      - Static application security testing (SAST) and secrets detection
                      - Findings in json file
                    link:
                      href: /pricing/
                      text: Get Started
                      data_ga_name: pricing
                      data_ga_location: free tier
                      aria_label: free tier
                  - id: premium
                    title: Premium
                    items:
                      - Static application security testing (SAST) and secrets detection
                      - Findings in json file
                      - MR approvals and more common controls
                    link:
                      href: /pricing/
                      text: Learn more
                      data_ga_name: pricing
                      data_ga_location: premium tier
                      aria_label: premium tier
                  - id: ultimate
                    title: Ultimate
                    items:
                      - Everything in Premium plus
                      - Comprehensive security scanners include SAST, DAST, Secrets, dependencies, containers, IaC, APIs, cluster images, and fuzz testing
                      - Actionable results within the MR pipeline
                      - Compliance pipelines
                      - Security and Compliance dashboards
                      - Much more
                    link:
                      href: /pricing/
                      text: Learn more
                      data_ga_name: pricing
                      data_ga_location: ultimate tier
                      aria_label: ultimate tier
                    cta:
                      href: /free-trial/
                      text: Try Ultimate for Free
                      data_ga_name: pricing
                      data_ga_location: ultimate tier
                      aria_label: ultimate tier
        - name: 'div'
          id: 'resources'
          slot_enabled: true
          slot_content:
          - name: solutions-resource-cards
            offset: 2
            data:
              title: Related Resources
              align: left
              column_size: 4
              grouped: true
              cards:
                - icon:
                    name: video
                    variant: marketing
                    alt: Video Icon
                  event_type: "Video"
                  header: Shifting Security Left - DevSecOps Overview
                  link_text: "Watch now"
                  image: "/nuxt-images/solutions/dev-sec-ops/shifting-security-left.jpeg"
                  href: "https://www.youtube.com/embed/XnYstHObqlA"
                  data_ga_name: Shifting Security Left - DevSecOps Overview
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: Video Icon
                  event_type: "Video"
                  header: Managing Vulnerabilities and Enabling Separation of Duties with GitLab
                  link_text: "Watch now"
                  image: /nuxt-images/solutions/dev-sec-ops/managing-vulnerabilities.jpeg
                  href: https://www.youtube.com/embed/J5Frv7FZtnI
                  data_ga_name: Managing Vulnerabilities and Enabling Separation of Duties with GitLab
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: Video Icon
                  event_type: "Video"
                  header: GitLab 15 Release - New Security Features
                  link_text: "Watch now"
                  image: "/nuxt-images/solutions/dev-sec-ops/gitlab-15-release.jpeg"
                  href: https://www.youtube.com/embed/BasGVNvOFGo
                  data_ga_name: GitLab 15 Release - New Security Features
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: Video Icon
                  event_type: "Video"
                  header: SBOM and Attestation
                  link_text: "Watch now"
                  image: "/nuxt-images/solutions/compliance/sbom-and-attestation.jpeg"
                  href: https://www.youtube.com/embed/wX6aTZfpJv0
                  data_ga_name: SBOM and Attestation
                  data_ga_location: resource cards
                - icon:
                    name: ebook
                    variant: marketing
                    alt: Ebook Icon
                  event_type: "Book"
                  header: "Guide to software supply chain security"
                  link_text: "Learn more"
                  image: "/nuxt-images/resources/resources_13.jpeg"
                  href: https://cdn.pathfactory.com/assets/10519/contents/360915/35d042c6-3449-4d50-b2e9-b08d9a68f7a1.pdf
                  data_ga_name: "Guide to software supply chain security"
                  data_ga_location: resource cards
                - icon:
                    name: ebook
                    variant: marketing
                    alt: Ebook Icon
                  event_type: "Book"
                  header: " GitLab DevSecOps survey"
                  link_text: "Learn more"
                  image: "/nuxt-images/resources/resources_1.jpeg"
                  href: https://cdn.pathfactory.com/assets/10519/contents/432983/c6140cad-446b-4a6c-96b6-8524fac60f7d.pdf
                  data_ga_name: " GitLab DevSecOps survey"
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: Blog Icon
                  event_type: "Blog"
                  header: "Ultimate guide to software supply chain security "
                  link_text: "Learn more"
                  href: https://about.gitlab.com/blog/2022/08/30/the-ultimate-guide-to-software-supply-chain-security/
                  image: /nuxt-images/blogimages/hotjar.jpg
                  data_ga_name: "Ultimate guide to software supply chain security "
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: Blog Icon
                  event_type: "Blog"
                  header: "Comply to NIST framework with GitLab "
                  link_text: "Learn more"
                  href: https://about.gitlab.com/blog/2022/03/29/comply-with-nist-secure-supply-chain-framework-with-gitlab/
                  image: "/nuxt-images/resources/resources_7.jpeg"
                  data_ga_name: "Comply to NIST framework with GitLab "
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: Blog Icon
                  event_type: "Blog"
                  header: "Elite team strategies to secure the software supply chain "
                  link_text: "Learn more"
                  href: https://about.gitlab.com/blog/2022/01/06/elite-team-strategies-to-secure-software-supply-chains/
                  image: "/nuxt-images/resources/resources_13.jpeg"
                  data_ga_name: "Elite team strategies to secure the software supply chain "
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: Blog Icon
                  event_type: "Blog"
                  header: "Securing the software supply chain through automated attestation"
                  link_text: "Learn more"
                  href: https://about.gitlab.com/blog/2022/08/10/securing-the-software-supply-chain-through-automated-attestation/
                  image: "/nuxt-images/resources/resources_4.jpeg"
                  data_ga_name: "Securing the software supply chain through automated attestation"
                  data_ga_location: resource cards
                - icon:
                    name: report
                    alt: Report Icon
                    variant: marketing
                  event_type: "Analyst Report"
                  header: "GitLab a challenger in 2022 Gartner Magic Quadrant"
                  link_text: "Learn more"
                  href: "https://about.gitlab.com/analysts/gartner-ast22/"
                  image: /nuxt-images/blogimages/hemmersbach_case_study.jpg
                  data_ga_name: "GitLab a challenger in 2022 Gartner Magic Quadrant"
                  data_ga_location: resource cards
    - name: solutions-cards
      data:
        title: Do more with GitLab
        column_size: 4
        link :
          url: /solutions/
          text: Explore more Solutions
          data_ga_name: soluions explore more
          data_ga_location: body
        cards:
          - title: DevSecOps
            description: GitLab empowers your teams to balance speed and security by automating software delivery and securing your end-to-end software supply chain. 
            icon:
              name: devsecops
              alt: devsecops Icon
              variant: marketing
            href: /solutions/dev-sec-ops/
            data_ga_name: devsecops learn more
            data_ga_location: body
          - title: Continuous Software Compliance
            description: Integrating security into your DevOps lifecycle is easy with GitLab.
            icon:
              name: build
              alt: build Icon
              variant: marketing
            href: /solutions/continuous-software-compliance/
            data_ga_name: continuous software compliance learn more
            data_ga_location: body
          - title: Continuous Integration and Delivery
            description: Make software delivery repeatable and on-demand
            icon:
              name: continuous-delivery
              alt: Continuous Delivery
              variant: marketing
            href: /features/continuous-integration/
            data_ga_name: siemens learn more
            data_ga_location: body

                  