---
  title: Why GitLab?
  description: "Don't only take our word for it! GitLab is the only comprehensive DevOps platform being delivered as a single application. Learn more here!"
  jump_to: "#sdlc-features"
  hero:
    aos_animation: fade-up
    aos_duration: 400
    header: Why GitLab
    subheader: The One DevOps Platform
    primary_button:
      link: "https://gitlab.com/-/trials/new?glm_content=default-saas-trial&glm_source=about.gitlab.com"
      data_ga_name: "free trial"
      data_ga_location: "why cta"
      text: "Get free trial"
    secondary_button:
      data_ga_name: "watch video"
      data_ga_location: "why cta"
      text: "What is GitLab?"
      modal:
        video_link: https://player.vimeo.com/video/702922416?h=06212a6d7c
  loop:
    title: Simplify software development with The One DevOps Platform
    aos_animation: fade-up
    aos_duration: 1500
    aos_delay: 800
    blocks:
      - title: Instead of pulling together point solutions for every step in the lifecycle
      - title: And constantly juggling all those tools and homegrown scripts
      - title: Your team can focus on developing and delivering the code that matters
      - title: It's time for a complete solution that does it all
    cta_button:
      text: Get free trial
      url: https://gitlab.com/-/trials/new?glm_content=default-saas-trial&glm_source=about.gitlab.com
      data_ga_name: free trial
      data_ga_location: body
  sdlc_table_block:
    title: The One DevOps Platform for software innovation
    table:
      - header: Plan
        icon:
          name: plan-light
          alt: Plan Icon
          variant: marketing
        url: /stages-devops-lifecycle/plan/
        subgroups:
          - title: Team Planning
            url: https://docs.gitlab.com/ee/topics/plan_and_track.html#team-planning
          - title: Portfolio Management
            url: /stages-devops-lifecycle/portfolio-management/
          - title: Service Desk
            url: /stages-devops-lifecycle/service-desk/
          - title: Requirements Management
            url: https://docs.gitlab.com/ee/user/project/requirements/
          - title: Quality Management
            url: https://docs.gitlab.com/ee/ci/test_cases/index.html
          - title: Design Management
            url: https://docs.gitlab.com/ee/user/project/issues/design_management.html
      - header: Create
        icon:
          name: create-light
          alt: Create Icon
          variant: marketing
        url: /stages-devops-lifecycle/create/
        subgroups:
          - title: Source Code Management
            url: /stages-devops-lifecycle/source-code-management/
          - title: Code Review
            url: /stages-devops-lifecycle/code-review/
          - title: Wiki
            url: https://docs.gitlab.com/ee/user/project/wiki/
          - title: Web IDE
            url: https://docs.gitlab.com/ee/user/project/web_ide/index.html
          - title: Snippets
            url: https://docs.gitlab.com/ee/user/snippets.html
      - header: Verify
        icon:
          name: verify-light
          alt: Verify Icon
          variant: marketing
        url: /stages-devops-lifecycle/verify/
        subgroups:
          - title: Continuous Integration (CI)
            url: /features/continuous-integration/
          - title: Code Testing and Coverage
            url: https://docs.gitlab.com/ee/ci/unit_test_reports.html
          - title: Performance Testing
            url: https://docs.gitlab.com/ee/user/project/merge_requests/browser_performance_testing.html
          - title: Merge Trains
            url: https://docs.gitlab.com/ee/ci/pipelines/merge_trains.html
          - title: Review Apps
            url: /stages-devops-lifecycle/review-apps/
          - title: Secrets Management
            url: https://docs.gitlab.com/ee/ci/secrets/
      - header: Package
        icon:
          name: package-light
          alt: Package Icon
          variant: marketing
        url: /stages-devops-lifecycle/package/
        subgroups:
          - title: Package Registry
            url: https://docs.gitlab.com/ee/user/packages/package_registry/
          - title: Container Registry
            url: https://docs.gitlab.com/ee/user/packages/container_registry/
          - title: Helm Chart Registry
            url: https://docs.gitlab.com/ee/user/packages/container_registry/#use-the-container-registry-to-store-helm-charts
          - title: Dependency Proxy
            url: https://docs.gitlab.com/ee/user/packages/dependency_proxy/
          - title: Git LFS
            url: https://docs.gitlab.com/ee/topics/git/lfs/index.html
      - header: Secure
        icon:
          name: secure-light
          alt: Secure Icon
          variant: marketing
        url: /stages-devops-lifecycle/secure/
        subgroups:
          - title: SAST
            url: https://docs.gitlab.com/ee/user/application_security/sast/
          - title: Secret Detection
            url: https://docs.gitlab.com/ee/user/application_security/secret_detection/
          - title: Code Quality
            url: https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html
          - title: DAST
            url: https://docs.gitlab.com/ee/user/application_security/dast/
          - title: API Security
            url: https://docs.gitlab.com/ee/user/application_security/dast_api/
          - title: Fuzz Testing
            url: https://docs.gitlab.com/ee/user/application_security/coverage_fuzzing/
          - title: Dependency Scanning
            url: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/
          - title: Container Scanning
            url: https://docs.gitlab.com/ee/user/application_security/container_scanning/
          - title: License Compliance
            url: https://docs.gitlab.com/ee/user/compliance/license_compliance/index.html
      - header: Release
        icon:
          name: release-light
          alt: Release Icon
          variant: marketing
        url: /stages-devops-lifecycle/release/
        subgroups:
          - title: Continuous Delivery
            url: /features/continuous-integration/
          - title: Pages
            url: /stages-devops-lifecycle/pages/
          - title: Advanced Deployments
            url: https://docs.gitlab.com/ee/topics/autodevops/index.html#incremental-rollout-to-production-premium
          - title: Feature Flags
            url: https://docs.gitlab.com/ee/operations/feature_flags.html
          - title: Release Evidence
            url: https://docs.gitlab.com/ee/user/project/releases/#release-evidence
          - title: Release Orchestration
            url: https://docs.gitlab.com/ee/user/project/releases/
          - title: Environment Management
            url: https://docs.gitlab.com/ee/ci/environments/
      - header: Configure
        icon:
          name: configure-light
          alt: Configure Icon
          variant: marketing
        url: /stages-devops-lifecycle/configure/
        subgroups:
          - title: Auto DevOps
            url: /stages-devops-lifecycle/auto-devops/
          - title: Kubernetes Management
            url: /solutions/kubernetes/
          - title: Deployment Management
            url: https://docs.gitlab.com/ee/topics/release_your_application.html
          - title: ChatOps
            url: https://docs.gitlab.com/ee/ci/chatops/
          - title: Infrastructure as Code
            url: https://docs.gitlab.com/ee/user/infrastructure/iac/index.html
      - header: Monitor
        icon:
          name: monitor-light
          alt: Monitor Icon
          variant: marketing
        url: /stages-devops-lifecycle/monitor/
        subgroups:
          - title: Metrics
            url: https://docs.gitlab.com/ee/operations/metrics/
          - title: Incident Management
            url: https://docs.gitlab.com/ee/operations/incident_management/
          - title: On-call Schedule Management
            url: https://docs.gitlab.com/ee/operations/incident_management/oncall_schedules.html
          - title: Tracing
            url: https://docs.gitlab.com/ee/operations/tracing.html
          - title: Error Tracking
            url: https://docs.gitlab.com/ee/operations/error_tracking.html
          - title: Product Analytics
            url: https://docs.gitlab.com/ee/operations/product_analytics.html
      - header: Govern
        icon:
          name: protect-light
          alt: Govern Icon
          variant: marketing
        url: /stages-devops-lifecycle/govern/
        subgroups:
          - title: Security Policies
            url: https://docs.gitlab.com/ee/user/application_security/
          - title: Vulnerability Management
            url: https://docs.gitlab.com/ee/user/application_security/security_dashboard/
          - title: Dependency Management
            url: https://docs.gitlab.com/ee/user/application_security/dependency_list/
          - title: Audit Events
            url: https://docs.gitlab.com/ee/administration/audit_events.html
          - title: Compliance Management
            url: https://docs.gitlab.com/ee/administration/compliance.html
      - header: Manage
        icon:
          name: manage-light
          alt: Manage Icon
          variant: marketing
        url: /stages-devops-lifecycle/manage/
        subgroups:
          - title: Subgroups
            url: https://docs.gitlab.com/ee/user/group/subgroups/
          - title: DevOps Reports
            url: https://docs.gitlab.com/ee/user/admin_area/analytics/dev_ops_report.html
          - title: Value Stream Management
            url: /solutions/value-stream-management/
  showcase:
    data:
      title: What makes GitLab different
      items:
        - title: It covers all stages of the DevOps lifecycle
          icon:
            name: automated-code
            alt: Automated Code Icon
            variant: marketing
          text: From managing and planning to deploying and monitoring, GitLab has you covered.
          link:
            text: Learn more
            href: https://about.gitlab.com/stages-devops-lifecycle/
            data_ga_name: Stages DevOps Lifecycle
            data_ga_location: body
        - title: All the essential DevOps capabilities in one application
          icon:
            name: community
            alt: Community Icon
            variant: marketing
          text: GitLab gives you everything from value stream reporting to planning tools, registries, CI/CD, testing, and much more.
          link:
            text: Learn more
            href: https://about.gitlab.com/features/
            data_ga_name: GitLab features
            data_ga_location: body
        - title: Cloud agnostic and deployment agnostic
          icon:
            name: gitlab-cloud
            alt: GitLab Cloud Icon
            variant: marketing
          text: You can use GitLab how and where you want to fit with your digital infrastructure.
        - title: SaaS and self-managed options
          icon:
            name: manage-alt-2
            alt: Manage Icon
            variant: marketing
          text: "Choose what’s best for your organization: SaaS or self-managed. We can host and manage GitLab for you, or you can deploy your own GitLab instance on-premises or in the cloud."
          link:
            text: Learn more
            href: https://about.gitlab.com/handbook/marketing/strategic-marketing/dot-com-vs-self-managed/
            data_ga_name: SaaS vs self-managed
            data_ga_location: body
        - title: Security and compliance are built in
          icon:
            name: lock-alt-5
            alt: Lock Icon
            variant: marketing
          text: With GitLab, you can automate your security and compliance policies and you’ll get visibility and traceability to see who changed what, where and when across all DevOps functions.
        - title: A platform for all to collaborate
          icon:
            name: visibility
            alt: Visibility Icon
            variant: marketing
          text: GitLab was built for Dev, Sec, Ops and everyone else who cares about your code — including business teams and non-technical stakeholders — to collaborate and keep moving projects forward.

        - title: It’s open and always improving
          icon:
            name: open-source
            alt: Open Source Icon
            variant: marketing
          text: Because GitLab is built on open source software, you get the benefit of all the innovations that thousands of developers all over the world are continuously adding and refining — and you can contribute your own.
  growth_block:
    header: Enterprise-scale solutions
    accordion:
      - header: Source code management (SCM)
        text: |
          - Git repository with version control and collaboration
        link_url: https://about.gitlab.com/stages-devops-lifecycle/source-code-management/
        data_ga_name: scm learn more
        data_ga_location: body
      - header: Continuous integration and deployment (CI/CD)
        text: |
          - Consistent, automated pipelines and multi-cloud deployment
        link_url: https://docs.gitlab.com/ee/ci/
        data_ga_name: cicd learn more
        data_ga_location: body
      - header: Software security and compliance
        text: |
          - Shift both security and compliance left
          - Find and fix flaws earlier
          - Manage vulnerabilities more efficiently
      - header: GitOps & infrastructure automation
        text: |
          - Automation and collaboration for cloud-native, multi-cloud, and legacy environments
        link_url: https://about.gitlab.com/solutions/gitops/
        data_ga_name: gitops learn more
        data_ga_location: body
      - header: Governed software supply chain
        text: |
          - End-to-end policy automation, enforcement, and traceability
          - Built in common controls for compliance
          - Guardrails for unobtrusive policy enforcement
    customer_logos_block:
      - image_url: "/nuxt-images/home/logo_goldman_sachs_mono.svg"
        link_label: Link to Goldman Sachs customer case study
        alt: "Goldman Sachs logo"
        url: "/customers/goldman-sachs/"
      - image_url: "/nuxt-images/home/logo_siemens_mono.svg"
        link_label: Link to Siemens customer case study
        alt: "Siemens logo"
        url: /customers/siemens/
      - image_url: "/nuxt-images/home/logo_nvidia_mono.svg"
        link_label: Link to Nvidia customer case study
        alt: "Nvidia logo"
        url: /customers/Nvidia/
      - image_url: "/nuxt-images/home/logo_eesa_mono.svg"
        link_label: Link to EESA customer case study
        alt: "eesa logo"
        url: /customers/european-space-agency/
      - image_url: "/nuxt-images/home/logo_cncf_mono.svg"
        link_label: Link to Cloud Native Computing Foundation customer case study
        alt: "Cloud Native logo"
        url: /customers/cncf/
      - image_url: "/nuxt-images/home/logo_ticketmaster_mono.svg"
        alt: "Ticketmaster logo"
  accolades_block:
    header: GitLab is leading the way
    link_text: See more
    link_url: https://about.gitlab.com/why-gitlab/
    data_ga_name: "customers see more"
    data_ga_location: "body"
    accolades:
      - header_action: Voted the
        header_content: Best DevOps Solution Provider
        text: GitLab was awarded the DevOps Dozen award for Best DevOps Solution Provider by industry leaders MediaOps and DevOps.com.
        link_url: https://devops.com/5th-annual-devops-dozen-winners-announced/
        data_ga_name: Voted the Best DevOps Solution Provider
        data_ga_location: "body"

      - header_action: Recognized as a
        header_content: 451 Firestarter
        text: GitLab received a 451 Firestarter award from leading technology research and advisory firm 451 Research, recognizing exceptional innovation within the information technology industry.
        link_url: https://about.gitlab.com/press/releases/2020-01-14-gitlab-recognized-as-451-firestarter.html
        data_ga_name: Recognized as a 451 Firestarter
        data_ga_location: "body"

      - header_action: Recognized as a
        header_content: Leader in the Forrester Wave™
        text: "Forrester evaluated GitLab as a Leader in Cloud-Native Continuous Integration in The Forrester Wave™: Cloud-Native Continuous Integration Tools, Q3 2019 report."
        link_url: https://about.gitlab.com/analysts/forrester-cloudci19/
        data_ga_name: Recognized as a Leader in the Forrester Wave
        data_ga_location: "body"
  solution_comparison:
    title: GitLab vs. GitHub
    subtitle: Feature maturity comparison against set of industry standards
    link_text: Learn more
    link_href: /devops-tools/github-vs-gitlab/
    data_ga_name: Solution comparison learn more
    data_ga_location: "body"
    solutions:
      - solution_name: Plan
        competitor_percent: 37
        gitlab_percent: 65
      - solution_name: Create
        competitor_percent: 87
        gitlab_percent: 100
      - solution_name: Verify
        competitor_percent: 79
        gitlab_percent: 91
      - solution_name: Package
        competitor_percent: 77
        gitlab_percent: 81
      - solution_name: Secure
        competitor_percent: 53
        gitlab_percent: 97
      - solution_name: Release
        competitor_percent: 64
        gitlab_percent: 93
      - solution_name: Configure
        competitor_percent: 29
        gitlab_percent: 86
      - solution_name: Monitor
        competitor_percent: 0
        gitlab_percent: 33
      - solution_name: Govern
        competitor_percent: 0
        gitlab_percent: 100
      - solution_name: Manage
        competitor_percent: 60
        gitlab_percent: 80
  case_studies:
    header: Case Studies
    link_text: View all case studies
    link_url: https://about.gitlab.com/customers/
    data_ga_name: case studies view all
    data_ga_location: body
    cards:
      - icon: /nuxt-images/why/case_study_icon.svg
        event_type: Case Study
        header: Goldman Sachs improves from one build every two weeks to over a thousand per day
        link_text: Read more
        href: https://about.gitlab.com/customers/goldman-sachs/
        image: /nuxt-images/why/case_study_image_option1.png
        data_ga_name: Case Study - Goldman Sachs
        data_ga_location: body

      - icon: /nuxt-images/why/case_study_icon.svg
        event_type: Case Study
        header: Axway aims for elite DevOps status with GitLab
        link_text: Read more
        href: https://about.gitlab.com/customers/axway-devops/
        image: /nuxt-images/why/case_study_image_option2.png
        data_ga_name: Case Study - Axway
        data_ga_location: body

      - icon: /nuxt-images/why/case_study_icon.svg
        event_type: Case Study
        header: Hemmersbach increased build speed by 59 times
        link_text: Read more
        href: https://about.gitlab.com/customers/hemmersbach/
        image: /nuxt-images/why/case_study_image_option3.png
        data_ga_name: Case Study - Hemmersbach
        data_ga_location: body
